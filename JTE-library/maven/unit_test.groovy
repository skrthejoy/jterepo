void call(){
		try
		{
			sh 'mvn test'
			echo 'Test-Completed'
			junit 'target/surefire-reports/*.xml'
			
		}
		catch(e)
		{
			echo 'Test-Execution-Failed'
			//throw e //Since we are catching the exception we can rethrow it to show the stage has failed
		}
		finally
		{
			echo 'This will run always'
		}
}